/**
 * For the purpose of this demo since it is only to demonstrate a simple page in
 * AngularJS
 * 
 * By: Leo Teng
 * Date: 2015-12-09
 */
var demoApp = angular.module('demoApp', [ 'ngRoute', 'ngAnimate' ]) //Loaded dependencies but not used. Likely needed but for demo purpose, not implemented.

/**
 * Since this demo is not likely to get hosted. I will quickly mock up a simply
 * routing tree but comment it out
 * 
 * NewsController and news.html (index.html in this demo) will be the index page for news page, user can navigate between new categories
 * 
 * News page then loads different category and their relative contents through a ng-view. 
 * If using ui-router, ui-view, or simply a named view in same state.
 * 
 * Ideally, news page should be a sub state of a application and catogery pages are sub state of news state
 */

//.config(function($routeProvider) {
//  $routeProvider.when('/news', {
//    controller : 'NewsController',
//    templateUrl : 'news.html'
//  }).when('/news/{{categoryId}}', {
//    controller : 'CategoryController',
//    templateUrl : './category-view.html'
//  })
//})
