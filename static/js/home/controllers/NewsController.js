(function(){
  'use strict';
  angular.module('demoApp').controller('NewsController', function($scope, newsService){
    $scope.categories = newsService.getCategories();
    $scope.currentCategory = $scope.categories[$scope.categories.length-1]; //For demo purpose
    
    //Click event handler for clicking a category
    //Here is where we switch category view or state depends on our we set it up
    
    $scope.chooseCategory = function(event, category){
      console.log(category, event);
      if ($scope.currentCategory){
        $scope.currentCategory.active = false;
      }
      $scope.currentCategory = category;
      $scope.currentCategory.active = true;
      //Code for switch route or show view
    }
    
    
    //Also retrived through ajax, using service
    
    $scope.news = {};
    $scope.images = [];
    newsService.getNewsByCategoryId($scope.currentCategory.id).then(function(newsStory){
      $scope.news = newsStory;
      
      //Copy content for safe modifitication
      $scope.images = angular.copy($scope.news.images);
      angular.forEach($scope.images, function(image){
        image.active = false;
      });
      $scope.images[0].active = true;
    });
    
    
    
  })
  
})();