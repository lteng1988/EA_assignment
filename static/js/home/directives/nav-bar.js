/**
 * A generic directive to display a list of items as nav bar
 * Using Bootstrap CSS
 * 
 * @param navigation items - a list of navigational object containing links and properties
 * @param [selectFn] function - callback when clicked on the link
 */

(function() {
  'use strict';
  angular.module('demoApp').directive('navBar', function() {
    return {
      restrict: 'EA',
      replace : true,
      scope : {
        navItems: '=navBar',
        selectFn: '&selectFn'
      },
      templateUrl: 'nav-bar.html',
      controller: function($scope){
        $scope.select = function(event, item){
          $scope.selectFn({
            item: item,
            event: event
          });
        }
      }
    };
  });
  
  //Since this app is not likely to get hosted, I will put the template in templateCache to avoid retriving using HTTP
  //The actual HTML template is also include in the same folder
  angular.module('demoApp').run(function($templateCache) {
    $templateCache.put('nav-bar.html', '<nav class="nav-container"> <ul class="nav nav-pills"> <li role="presentation" class="pointer inline nav-item" ng-repeat="item in navItems" ng-class="{\'active\': item.active}"><!-- For demo purpose removed the href --><a ng-href="" ng-click="select(event, item)">{{item.name}}</a></li></ul> <hr/></nav>');
    
  });
  
})();