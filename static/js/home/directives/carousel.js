/**
 * A generic directive to display a list of images as a caoursel
 * 
 * I assume I am not allowed to use bootstrap carousel so this is a quick implementation
 * of a carousel. There can be much improvement made on this.
 * For example, animations. Left and right control.
 * 
 * For simplicty, this is hardcoded to fit 5 images for the following reason:
 * This is a carousel with preview images so we will run into issues
 *    of overflown preview images. Without proper design, it's hard to generalize this
 *    to fit n images. 
 *    E.g., do we show the middle 5 images with current image in the center?
 * 
 */

(function() {
  'use strict';
  angular.module('demoApp').directive('carousel', function($interval) {
    return {
      restrict: 'EA',
      replace : true,
      scope : {
        images: '=images',
        interval: '='
      },
      templateUrl: 'carousel.html',
      controller: function($scope){
        $scope.interval = $scope.interval || 3000; //Default to 3 seconds
        var interval = +$scope.interval;
        var currentImageIndex = 0;
        
        $scope.selectImage = function(index){
          $scope.images[currentImageIndex].active = false;
          currentImageIndex = index;
          $scope.images[currentImageIndex].active = true;
        }
        
        function rotateImage(images){
          images[currentImageIndex].active = false;
          currentImageIndex = (currentImageIndex + 1) % images.length;
          images[currentImageIndex].active = true;
        }
        
        $interval(function(){
          rotateImage($scope.images);
        }, interval);
      }
    };
  });
  
  //Since this app is not likely to get hosted, I will put the template in templateCache to avoid retriving using HTTP
  //The actual HTML template is also include in the same folder
  angular.module('demoApp').run(function($templateCache) {
    $templateCache.put('carousel.html', '<section class="carousel-container"> <figure class="active-image" ng-repeat="activeImage in images | filter:{active: true}| limitTo: 1" ng-animate> <img alt="{%{activeImage.title}%}" ng-src="{{activeImage.src}}"> <figcaption class="subtitle">{{activeImage.subtitle}}</figcaption> </figure> <div class="previews"> <figure class="pointer preview-image" ng-repeat="(index, image) in images | filter:{active: false}"> <img alt="{{image.title}}" ng-click="selectImage(index)" ng-src="{{image.src}}"> </figure> </div></section>');
  });
  
})();