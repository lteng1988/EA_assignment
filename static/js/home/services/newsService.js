/**
 * I choose to use news service to serve news content because:
 * 1. Data handling should resides in the service to avoid duplication of code
 * 2. Provides a centralized location to retrieve data
 * 3. Can easily implement some form of caching
 * 4. Permission checks
 * 5. Error handling
 */
(function() {
  'use strict';
  angular.module('demoApp').factory('newsService', function($http, $q) {
    var factory = {};
    /**
     * getNewsByCategoryId
     * @breif: this function get news contents base on category id from server
     * 
     * @param <Any> id - unique identifier for news category
     * @return <Promise> promise - promise resolve to object containing news story contents and propties
     */
    factory.getNewsByCategoryId = function(id){
      //For demo purpose
      //Normally will be returned by server
      var newsSummary = 'Star Wars Battle Front is going to be the best video game ever made. It is slated \
        to launch on November 17th, 2015. The grapgics look awesome, and you can fly man. How awesome is that. \
        Lorem Ipsum Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. \
        VEstibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet \
        quam egestas semper. Aenean ultricies mi vitae est. Mauris placeray eleifend leo.';
      
      //This is where we call to server and retrieve news content base on the category
      //We may or may not post process the news for pages to serve it
      //For simplicity, I hard coded contents
      var deferred = $q.defer();
      
      var newsStory = {
          title: 'Electronics Arts Releases the Best Video Game Ever Made',
          reporter: 'Preet S. Jassi',
          publishDate: 'Aug 1st, 2015', //For simplicity, I assume this is formatted nicely
          summary: newsSummary,
          images:  [
                    {
                      title: '2',
                      subtitle: 'Pew Pew Pew, dog fight!',
                      src: './static/images/battlefrontimages/featuredImage.img.2.jpg'
                    },
                    {
                      title: '3',
                      subtitle: 'This is awesome!',
                      src: './static/images/battlefrontimages/featuredImage.img.3.jpg'
                    },
                    {
                      title: '4',
                      subtitle: 'Yaaaaaaay',
                      src: './static/images/battlefrontimages/featuredImage.img.5.jpg'
                    },
                    {
                      title: '5',
                      subtitle: 'Cool',
                      src: './static/images/battlefrontimages/featuredImage.img.4.jpg'
                    },
                    {
                      title: '1',
                      subtitle: 'Yaaaaaaay',
                      src: './static/images/battlefrontimages/featuredImage.img.jpg'
                    }
                    ],
            //Ideally cards is a set of snipet news story. They are short and allow us to dynamically add them
            //Also, we can prioritize them very well
            //They can provide links to detail page as well
            //They can be easily organized on screen
            //In this demo, I treat the sub section with titles beside the image as news "cards"
            cards: [
                    {
                      title: 'Play As Iconic Star Wars Characters',
                      content: 'Never before have you been able to play as Darth Vader!. Pellentesque habitant morbi\
                        tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam,\
                        feugiat vitae, ultricies eget, tempor sit amet, ante.'
                    },
                    {
                      title: 'Engage in Dog Fights',
                      content: 'Never before have you been able to play as Darth Vader!. Pellentesque habitant morbi\
                        tristique senectus.'
                    }
                    ]
      };
      deferred.resolve(newsStory);
      return deferred.promise;
    }
    
    /**
     * getCategories
     * @brief retrieve a list of news categories to display, can have arbitary length
     *        contains properties and unique identifier id to retrieve the news story or some way to identify a piece of story
     */
    factory.getCategories = function(){
      //Will call to server to retrieve this information
      //For simplicity, I will hard code
      //Could have more properties
      return [{
        name: 'Canada',
        id: '',// Likely used for route
        active: false
      }, 
      {
        name: 'Word',
        id: '',// Likely used for route
        active: false
      },
      {
        name: 'U.S.',
        id: '',// Likely used for route
        active: false
      },
      {
        name: 'Politics',
        id: '',// Likely used for route
        active: false
      },
      {
        name: 'Business',
        id: '',// Likely used for route
        active: false
      },
      {
        name: 'Sports',
        id: '',// Likely used for route
        active: false
      },
      {
        name: 'Tech',
        id: '',// Likely used for route,
        active: true
      }];
    }
    return factory;
  });
})();
